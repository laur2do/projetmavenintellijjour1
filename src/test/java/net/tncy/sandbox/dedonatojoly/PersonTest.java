package net.tncy.sandbox.dedonatojoly;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author student
 */
public class PersonTest {

    private static Validator validator;

    public PersonTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void validateWelldefinedPerson() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1974, Calendar.DECEMBER, 3);
        Date birthDate = calendar.getTime();
        Person p = new Person("Xavier", "Roy", birthDate, "FR");
        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p);
        assertEquals(0, constraintViolations.size());
    }

    @Test
    public void validateUndefinedPerson() {
        Person p = new Person();
        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p);
        assertEquals(4, constraintViolations.size());
    }

    @Test
    public void validateAnonymous() {
        Person p = new Person("", "", null, "");
        Set<ConstraintViolation<Person>> constraintViolations = validator.validate(p);
        assertEquals(4, constraintViolations.size());
    }

}
